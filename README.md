# Portfolio QA

**About me**

Katherin is an experienced QA engineer with an Engineering Degree in Computer Science. 
She has almost 5 years of progressive experience in IT operations, almost 2 years in system implementation and analysis activities and over 3 years in QA.
She has worked with Agile methodologies such as Kanban and Scrum over the last few years but is able to adapt to any methodology. 
Katherin is a good communicator in English and can write and speak English at a conversational level. She believes that her greatest contribution to a project is her ability to learn quickly, adapt to the work environment and deliver quality results.

**Known Technologies**

Javascript, Java, Python, ASP.net MVC, .NET (#C), MySQL, SQL Server, Oracle, HTML, CSS, Jquery, Django, CI/CD, PowerBI, Crystal Reports, Postman, Jmeter, Blazemeter, Selenium IDE, Selenium WebDriver, TestNG, Cypress, Zeplin, Figma, Git, Gitlab, Github, Jira 

**Courses**

• Agile with Atlassian Jira (Coursera) 

• Advanced TestNG Framework and Integration with Selenium (Coursera) 

• Testing and Debugging Python (Coursera) 

• Continuous delivery and DevOps (Coursera) 

• Database Foundations Course (Oracle Academy) 

• Database Design Course (Oracle Academy) 

• Database Programming with SQL Course (Oracle Academy) 

• Database Programming with PL/SQL Course (Oracle Academy) 
